import java.io.*;
import java.util.*;
import java.io.FileNotFoundException;
import java.util.Arrays;
public class MyFile {

public static void main (String[] args){
    //initialize variables
    String[] store;
    String operation, numbers;
    String[] numArray;
    int index = 0;
    int sum = 0;
    int count = 0;
    String output = "";
    String output2 = "";
    String output3 = "";
    String output4 = "";
    String output5 = "";

    //try block to read file
    try
    {
        //open file and create output file
        File x = new File("input.txt");
        Scanner sc = new Scanner(x);
        Formatter f = new Formatter("output.txt");

        //while loop to scan file
        while (sc.hasNext()){
            //sotre info in fil and split along , and :
            store = sc.next().split(":");
            operation = store[0];
            numbers = store[1];
            numArray = numbers.split(",");
            int [] intNumArray = new int[numArray.length];

            if (index == numArray.length) {
                index = 0;
            }
            //reset sum
            sum = 0;
            index = 0;
            //for loop to create an integer array
            for(String i : numArray){

                intNumArray[index] = Integer.parseInt(i);

                if(operation.equals("")){
                    count++;
                }
                index++;
                sum = sum + Integer.parseInt(i);
                }

            //switch statement to perform operations.
            switch(operation){

                case "min":
                    output = "The min is: " + getSmallest(intNumArray,intNumArray.length) + "\n";
                    f.format(output);
                    break;
                case "max":
                    output2 = "The max is: " + getLargest(intNumArray,intNumArray.length)+ "\n";
                    f.format(output2);
                    break;
                case "avg":
                    output3 = "The average is "+ (float)sum/numArray.length + "\n";
                    f.format(output3);
                    break;
                case "sum":
                    output4 = "the sum is " + sum + "\n";
                    f.format(output4);
                    break;
                default:
                    operation = operation.substring(1);
                    double result = (Double.parseDouble(operation)/100)*numArray.length;
                    output5= operation +"th Percentile: " + result +"\n";
                    f.format(output5);
                    break;
            }
            System.out.println(sum);
        }
        f.close();
    }catch(FileNotFoundException e){

        }

    }
    //method to get max
    public static int getLargest(int[] a, int total){
        Arrays.sort(a);
        return a[total-1];
    }
    //method to get min
    public static int getSmallest(int[] a, int total){
        int temp;
        for (int i = 0; i < total; i++)
        {
            for (int j = i + 1; j < total; j++)
            {
                if (a[i] > a[j])
                {
                    temp = a[i];
                    a[i] = a[j];
                    a[j] = temp;
                }
            }
        }
        return a[0];
    }

}